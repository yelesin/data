#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import telebot
import  datetime
from telebot import types

dayStart = datetime.date(2021, 2, 22)


timeTableNotEven = {0 : "1) ----\n2) Методы вычислений 45 общ. (Гришин)\n3) Комп. схемотехника 45 общ. (Шугайло)\n4) Комп. схемотехника 72 (Шугайло)", 
                     1 : "1) ----\n2) Системное программирование 45 общ. (Трубина)\n3) Архитектура комп. 43 общ. (Берков)\n4) Архитектура комп. 72 (Якимчук)",
                     2 : "1) Українська  мова 45 общ. (Вдовиченко)\n2) Українська  мова !c 4 недели! 45 (Вдовиченко)\n3) Основы Web 72 (Якимчук)\n4) Английский 45 (Ткаченко)",
                     3 : "1) ----\n2) Системное программирование 38 (Трубина)\n3) Теория кодировки 45 общ. (Баранец) \n4) Системное программирование 76 (Трубина)", 
                     4 : "1) Методы вычислений 74 (Гришин)\n2) ----\n3) Физ-ра ПОЗАКРЕДИТНО 12:00 стадион\n4) ----",
                     5 : "Выходной",
                     6 : "Выходной"}

timeTableEven = {0 : "1) ----\n2) Методы вычислений 45 общ. (Гришин)\n3) Комп. схемотехника 45 общ. (Шугайло)\n4) ----", 
                     1 : "1) ----\n2) Системное программирование 45 общ. (Трубина)\n3) Основы Web 72 общ. (Якимчук)\n4) Архитектура комп. 72 (Якимчук)",
                     2 : "1) Українська  мова 45 общ. (Вдовиченко)\n2) Українська  мова 45 !c 4 недели! (Вдовиченко)\n3) Основы Web 72 (Якимчук)\n4) ----",
                     3 : "1) ----\n2) Системное программирование 38 (Трубина)\n3) Теория кодировки 45 общ. (Баранец) \n4) Системное программирование 76 (Трубина)", 
                     4 : "1) ---- \n2) ----\n3) Физ-ра ПОЗАКРЕДИТНО 12:00 стадион\n4) Охрана труда Шампанский переулок 3 общ.(Пенов/Бурденюк)",
                     5 : "Выходной",
                     6 : "Выходной"}
                    
days= ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье' ]
num_days=[0,0,7,31,30,31,30,31,31,30,31,30,31]

bot = telebot.TeleBot('1381086514:AAFxiqNRgZFfe0jvRjjf01chhpufM8pKe4w')

@bot.message_handler(commands=['start'])
def start(message):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    button_td = types.KeyboardButton(text="/td")
    button_tm = types.KeyboardButton(text="/tm")
    button_all = types.KeyboardButton(text="/all")
    keyboard.add(button_td, button_tm, button_all)
    
    user_id = message.chat.id
    bot.send_message(user_id, "/td - расписание на сегодня, /tm - расписание на завтра, /all - всё расписание, общ. - общаяя пара", reply_markup=keyboard)
    
@bot.message_handler(commands=['td'])
def SendTimeTable (message, tomorrow = False):
    evenFlag = False
    now =  datetime.date.today() 
    if tomorrow and now.weekday() == 6:
        weekday = 0
    elif tomorrow:
        weekday = now.weekday() + 1
    else:
        weekday = now.weekday()
    
    if(now.month == 2):
       diff = dayStart.day - now.day
       if(diff > 7):
           evenFlag = True
           
       if not evenFlag:       
           bot.send_message(message.chat.id, days[weekday] + ':\n' +  timeTableNotEven[weekday])
       else:
           bot.send_message(message.chat.id, days[weekday] + ':\n' + timeTableEven[weekday])
    else:
        all_days = 0
        for i in range(2, now.month):
            all_days += num_days[i]
        all_days += now.day
        
        n_weeks = 0
        while all_days % 7 != 0:
            all_days -= 1 
        n_weeks = int(all_days / 7) + 1
        
        if(n_weeks % 2 != 0):
            bot.send_message(message.chat.id, days[weekday] + ':\n' +  timeTableNotEven[weekday])
        else:
           bot.send_message(message.chat.id, days[weekday] + ':\n' + timeTableEven[weekday])
           
@bot.message_handler(commands=['tm'])
def Tm(message):
    SendTimeTable(message, True)            
    
@bot.message_handler(commands=['all'])
def allTimeTable(message):
    tmpEven ='Чётная неделя\n'
    tmpNeven = 'Нечётная неделя\n'
    for i in range(0, 7):
        tmpEven += '\n' + days[i] + '\n' + timeTableEven[i] + '\n'
        tmpNeven += '\n' + days[i] + '\n' + timeTableNotEven[i] + '\n'
    bot.send_message(message.chat.id, tmpEven)
    bot.send_message(message.chat.id, tmpNeven)
            
    
bot.polling()
